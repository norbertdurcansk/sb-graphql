package dao;

import cz.sbspa.dao.AuthorDao;
import cz.sbspa.model.Author;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertSame;


/**
 * Author test.
 */
@RunWith(SpringRunner.class)
public class AuthorTest {
    private List<Author> authors;

    @Before
    public void loadAuthors() {
        authors = new ArrayList<>();
        Author author = new Author();
        author.setName("Author1");
        author.setId("1");
        authors.add(author);
    }

    @Test
    public void defaultValues() {
        AuthorDao tester = new AuthorDao(authors);
        if (tester.getAuthor("1").isPresent()) {
            Author testAuthor = tester.getAuthor("1").get();
            assertSame(testAuthor, this.authors.get(0));
        }
    }
}
