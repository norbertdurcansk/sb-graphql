package cz.sbspa.dao;

import cz.sbspa.model.Author;

import java.util.List;
import java.util.Optional;

/**
 * Author dao object.
 */
public class AuthorDao {
    public List<Author> authors;

    public AuthorDao(List<Author> authors) {
        this.authors = authors;
    }

    public Optional<Author> getAuthor(String id) {
        return authors.stream().filter(author -> id.equals(author.getId())).findFirst();
    }
}
