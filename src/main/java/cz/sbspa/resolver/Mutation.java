package cz.sbspa.resolver;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import cz.sbspa.dao.PostDao;
import cz.sbspa.model.Post;

/** GraphQL mutation object. */
public class Mutation implements GraphQLMutationResolver {
    private PostDao postDao;

    public Mutation(PostDao postDao) {
        this.postDao = postDao;
    }

    public Post writePost(String title, String text, String category, String authorId) {
        Post post = new Post();
        post.setText(text);
        post.setCategory(category);
        post.setTitle(title);
        post.setAuthorId(authorId);
        postDao.savePost(post);
        return post;
    }
}
