package cz.sbspa.resolver;

import com.coxautodev.graphql.tools.GraphQLResolver;
import cz.sbspa.dao.PostDao;
import cz.sbspa.model.Author;
import cz.sbspa.model.Post;

import java.util.List;

/**
 * Author resolver for additional requests.
 */
public class AuthorResolver implements GraphQLResolver<Author> {
    private PostDao postDao;

    public AuthorResolver(PostDao postDao) {
        this.postDao = postDao;
    }

    public List<Post> getPosts(Author author) {
        return postDao.getAuthorPosts(author.getId());
    }
}
