package cz.sbspa.resolver;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import cz.sbspa.dao.AuthorDao;
import cz.sbspa.dao.PostDao;
import cz.sbspa.model.Post;

import java.util.List;

/** Query GraphQL configuration. */
public class Query implements GraphQLQueryResolver {
    private PostDao postDao;

    public Query(PostDao postDao, AuthorDao authorDao) {
        this.postDao = postDao;
    }

    public List<Post> recentPosts(int count, int offset) {
        return postDao.getRecentPosts(count, offset);
    }
}
