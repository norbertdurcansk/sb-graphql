package cz.sbspa.resolver;

import com.coxautodev.graphql.tools.GraphQLResolver;
import cz.sbspa.dao.AuthorDao;
import cz.sbspa.model.Author;
import cz.sbspa.model.Post;

import java.util.Optional;

/**
 * Post resolver for additional requests.
 */

public class PostResolver implements GraphQLResolver<Post> {
    private AuthorDao authorDao;

    public PostResolver(AuthorDao authorDao) {
        this.authorDao = authorDao;
    }

    public Optional<Author> getAuthor(Post post) {
        return authorDao.getAuthor(post.getAuthorId());
    }
}
